# -------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# -------------------------------------------------------------------------------------------

# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false


#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-icl

# Port 51002
server.port=51002

# Module Abbreviation Name (preferably <= 3 characters and only Upper Case Alphabets A-Z)
# e.g. LMs (for Liquidity), IPSH (for Payments), CNR (for Collections & Receivables)...
# This is used for deriving:
# 	- ElasticSearch Index name as <moduleAbbr>-requests (e.g. lms-requests, ipsh-requests, cnr-requests)
#	- Deriving Destination names in Release Trigger and State Update handler modules
ModuleAbbr=ICL

# Header key and value, to be used as security token while calling APIs for this module
iGTBD-AtomicAPI-SharedKey=I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq

Redis.DB.Url=localhost:6379
Redis.DB.Password=
# if SSL set to true, Redis client will use standard JRE truststore for SSL connectivity
Redis.DB.SSL.Enabled=false
# "true" indicates hostname mistmatch exception should be avoided (test env specific)
Redis.DB.SSL.DisableHostNameVerification=false

# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

# Camel Messages to be logged at this level (Possible values: ERROR, WARN, INFO, DEBUG, TRACE, OFF)
CamelMessageLoggingLevel=DEBUG

# Release Batch Size
# It is max number of requests to be released per batch (Approved, Retry batch)
ReleaseBatchSize=200

#BACKEND WAS
# Use appropriate Websphere bootstrap port.
# In CF, this parameter should be set automatically as per Release service binding.
Backend.ValidateServiceUri=http://10.10.7.90:20009
Backend.ReleaseServiceUri=corbaloc::10.10.7.90:20009

# Represents DataCenter region, country the process events/data belongs to
DataCenter.Region=
DataCenter.Country=

# Represents comma separated multiple service key & function regex patterns, this module is supposed to process events/data related to
# Format :
#   product/subproduct:function, product/subproduct:function, ....
# Example :
#   lqdy/swp.*:.*, lqdy/exec:add
#       indicates all requests/events with
#           - all serviceKeys matching "lqdy/swp.*" format AND related functions matching ".*" format
#           OR
#           - all serviceKeys matching "lqdy/exec" AND related function matching "add"
#       will be accepted for processing.
Digital.ServiceKey.Patterns=lqdy/icl.*:.*

#################################################################################################
###### Event Publish Handler specific Configuration
#################################################################################################
# Name of the Apache Camel JMS Component (which is configured at Domain level),
# which is to be used by Commons package for publishing Events 
# For RabbitsMQ
MsgPubHandler.CamelComponent=rabbitmq

#############################################################################################
###### Release Retry Configuration
#############################################################################################
RelRetryCfgIdentifierKeys=ACC_REL_ANY,AGREEMENT_ANY

###
# Release Retry Config Identifier key based configuration
# 
# RelRetryCfgIdentifier.<identifierKey>.payloadType=
# RelRetryCfgIdentifier.<identifierKey>.requestType=
# RelRetryCfgIdentifier.<identifierKey>.maxReleaseRetry=
# Where,
#	<payloadType,requestType> together forms a unique key combination for providing related configuration 
#	payloadType = type of the payload e.g. SweepStructure, LoanAgreement or any
#	requestType = type of the request e.g. create, update, delete or any
# 	maxReleaseRetry = to indicate how many times this message should be retried for releasing before it is marked as Failed
#			(field is optional - default value will be applied)
#
# Configuration is applied in following order of priority, for a combination of <payloadType> and <requestType>:
# 1. <payloadType> and <requestType> exactly match with values provided here.
# 2. Else - <payloadType> and "any"
# 3. Else - "any" and <requestType>
# 4. Else - "any" and "any"
# 5. Else - default retry configuration is applied
#
###
RelRetryCfgIdentifier.ACC_REL_ANY.payloadType=AccountRelationship
RelRetryCfgIdentifier.ACC_REL_ANY.requestType=any
RelRetryCfgIdentifier.ACC_REL_ANY.maxReleaseRetry=


RelRetryCfgIdentifier.AGREEMENT_ANY.payloadType=Agreement
RelRetryCfgIdentifier.AGREEMENT_ANY.requestType=any
RelRetryCfgIdentifier.AGREEMENT_ANY.maxReleaseRetry=

# Backend server details (Websphere specific)
Backend.JMS.InitialContextFactory=com.ibm.websphere.naming.WsnInitialContextFactory
Backend.JMS.ProviderURL=${Backend.ReleaseServiceUri}
Backend.JMS.QueueConnectionFactory=PortalQCF


##################################################################
### Common Connector Configuration
##################################################################
Connector.ACC_REL_TXFMR.route=direct:TransformRelationshipRoute
Connector.AGREEMENT_TXFMR.route=direct:TransformAgreementRoute

##################################################################
### Release Connector Specific Configuration
##################################################################
#RelConnector.HTTP_CONNECTOR.route=direct:HttpConnectorRoute
RelConnector.JMS_CONNECTOR.route=direct:JmsConnectorRoute

### Backend Endpoints
# SweepStructure specific

# Release JMS endpoint (using specific queue e.g. TestQ)
RelConnector.JMS.endpoint=custom-jms:queue:InterfaceCallQ?disableReplyTo=true

### Routing Slips
# icl specific
RelConnector.AccRelAny.routingSlip=${Connector.ACC_REL_TXFMR.route},\${RelConnector.JMS_CONNECTOR.route},\${RelConnector.JMS.endpoint}
RelConnector.AgreementAny.routingSlip=${Connector.AGREEMENT_TXFMR.route},\${RelConnector.JMS_CONNECTOR.route},\${RelConnector.JMS.endpoint}

##################################################################
### Validation specific Connector routes
##################################################################
ValConnector.HTTP_CONNECTOR.route=direct:HttpConnectorRoute

### Backend Endpoints
# SweepStructure specific
# Validation Endpoint using Interface approach
#backend
ValConnector.ACC_REL_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/LMS/rest/v1/intercompanyloan/account-relationship?ApplicationName=LMS&Event=RestWSCall
ValConnector.AGREEMENT_HTTP.endpoint=jetty://${Backend.ValidateServiceUri}/LMS/rest/v1/intercompanyloan/agreement?ApplicationName=LMS&Event=RestWSCall

### Routing Slips
# icl specific
ValConnector.AccRelAny.routingSlip=${Connector.ACC_REL_TXFMR.route},\${ValConnector.HTTP_CONNECTOR.route},\${ValConnector.ACC_REL_HTTP.endpoint}
ValConnector.AgreementAny.routingSlip=${Connector.AGREEMENT_TXFMR.route},\${ValConnector.HTTP_CONNECTOR.route},\${ValConnector.AGREEMENT_HTTP.endpoint}

##################################################################
### Backend Instance specific configuration for Camel Component
##################################################################
# Message Broker which is to be used by Commons for Release Batch Triggers, State Update Events and Event Publisher
# Supported value - RabbitMQ
Digital.MsgBroker.Type=RabbitMQ
# This is used by Commons for connecting to RabbitMQ Server
Digital.RabbitMQ.Host=localhost
Digital.RabbitMQ.Port=5672
Digital.RabbitMQ.User=guest
Digital.RabbitMQ.Password=guest
Digital.RabbitMQ.VHost=/
# if SSL set to true, RMQ client will use standard JRE truststore for SSL connectivity.
# internally creates rmqTrustManager bean as default TrustManager for Camel producer/consumer endpoints
Digital.RabbitMQ.SSL.Enabled=false

#############################################################################################
###### Release Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel JMS Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Release Batch Trigger events 
RelTriggerHandler.CamelComponent=rabbitmq
RelTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Comma separated list of Payload Types, for which this module need to support running Release Batches
RelTriggerHandler.PayloadTypes=AccountRelationship,Agreement


#################################################################################################
###### State Update Handler specific Configuration
#################################################################################################
# Name of the Apache Camel JMS Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to B/E State Update events 
StateUpdHandler.CamelComponent=rabbitmq
StateUpdHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

#################################################################################################
### State Management controls for draft and initiate states for unhappy validation scenarios
#################################################################################################
# Flag to indicate if initiate request should be marked as failed (500) if validate call gets timed out. Defaults to false. 
StateMgmt.failInitiateOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if validate call gets timed out during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if there are validation failure (400,422) during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValFailure=true
#################################################################################################


#############################################################################################
###### Purge Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Purge Batch Trigger events 
PurgeTriggerHandler.CamelComponent=rabbitmq
PurgeTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Minimum number of days the data must be retained in State Store (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.MinRetentionDays=720
# Flag indicating whether purging of IN_PROGRESS requests from State Store is allowed or not (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.PurgeOfInProgressAllowed=false


# Comma separated list of Payload Types, for which this module need to support running Purge Batches
PurgeTriggerHandler.PayloadTypes=AccountRelationship,Agreement

# Used as sleep time (in milliseconds) between each message retry, when msg is requeue'd.
# Keep it commented, unless need to override default value.
#MsgRetrySleepTime=250

# Used as max time (in seconds) a message to be retried for in requeue mode
# Keep it commented, unless need to override default value.
#MsgRetryMaxTime=86400

#############################################################################################
###### Registering mgmt endpoints to Eureka
#############################################################################################
eureka.instance.statusPageUrlPath=${server.contextPath}${management.context-path}/info
eureka.instance.healthCheckUrlPath=${server.contextPath}${management.context-path}/health

##################################################################
### Zipkin specific options
##################################################################
spring.zipkin.enabled=false
spring.sleuth.enabled=false
#spring.sleuth.web.additionalSkipPattern=.*/getDetails|.*/updateDetails
#spring.sleuth.sampler.probability=1.0
#spring.zipkin.baseUrl=http://localhost:9411

# WAS JMX beans registration flag
spring.jmx.enabled=false