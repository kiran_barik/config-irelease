# Node environment
NODE_ENV=production
#------------------------------------------------------------------------------
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION START
#
#### E1. Entitlement interface :::: Optional ####
# Possible values: mock | active
# Default: active
# mock: Reads entitlement profile from mock_entl.js in <projectRoot/src> folder
# active: Connects to Entitlement Service
AUTH_MODE=active

#### E2. System user entitlement :::: Required ####
# Possible values: true | false
# Default: false
# true: Indicates that the component is to be considered a system user in entitlements (eg. Gazetteer)
# false: End user entitlements apply
#
SYSTEM_USER_AUTH=true

######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION START
#
#### Q1. Query store parameters used in development/test :::: Optional ####
# Possible values: true | false
# Default: true
# The query store is used as a convenience to load a query/variable set in Graphiql
#
# LOAD_QUERY_STORE=true

#### Q2. Default query set to be loaded :::: Optional ####
# If not provided no query will be loaded
#
DEFAULT_QUERY_LOAD=accounts

######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION START
#
#### P1. CF Service Name for Elastisearch cluster :::: Optional ####
# Default value is elasticsearch
# Provide a value if you wish to override
# This is a Quest instance level configuration and will apply to all
# component packakges which will retrieve their configurations
#
CF_SVC_NAME_ELASTICSEARCH=elasticsearch
#### P2. CF Service Name for specific package :::: Optional ####
# This is only applicable at an individual component package level
# The specific package will bind to the service name specified
# When provided in the Quest configuration serves as an override
#
# Overrides can be provided with a suffix
# ENTL - EntitlementsService
# ACCT - AccountServices
# ORG - OrgService
# FXRATE - FxRatesService
# BUSCAL - BusinessCalendarService
# REFDATA - ReferenceDataService
# LIQI - LiquidityICLService
# LIQS - LiquiditySweepsService
# LIQC - LiquidityCommonService
# PAY - PaymentsService
# CNR - CNRService
#
# Examples
# CF_SVC_NAME_ELASTICSEARCH_ENTL=entl-els-projection
# CF_SVC_NAME_ELASTICSEARCH_ORG=org-els-projection
#### P3. ES Connection string :::: Required ####
# This is only used in local development.
# Is not considered when deployed in CF
#
ES_CONNSTRING_NOTS=http://localhost:9200

#### P4. ES Server Partition for indexes :::: Optional ####
# Default value: N/A (empty)
# Indexes will be created/accessed with this prefix
# e.g. if Partition has a value of "DIT" then the index for Account Services will be "dit.acct"
#
ES_PARTITION=quest
#### P5. Response size (limit) for Search queries :::: Optional ####
# Default value: 2000
# Search queries will throttle the number of records retrieved based on this value
#
ES_QUERY_LIMIT=2000
#### P6. Response size (limit) for Aggregate queries :::: Optional ####
# Default value: 50
# Aggregate queries will throttle the number of buckets retrieved based on this value
#
ES_AGG_QUERY_LIMIT=50
#### P6. Timeout threshold in ms :::: Optional ####
# Default value: 30000 ms
#
ES_TIMEOUT=12000
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## E. REGISTRY CONFIGURATION ######## ::: SECTION START
#
#### E1. Enable discovery :::: Optional ###
# Possible values: true | false
# Default value: true
#
EUREKA_DISCOVERY_ENABLED=true
#### E2. Eureka discovery name :::: Required ####
# Default value: notif-dispatcher
#
EUREKA_DISCOVERY_NAME=notif-dispatcher
#### E3. Eureka service name :::: Optional ####
# Default value: digital-registry
# Eureka service name to connect for discovery services
#
CF_SVC_NAME_DIGITAL_REGISTRY=digital-registry
#### E4. Eureka discovery name :::: Required ####
# Default value: digital-quest
# If there are multiple quest contexts supported this must be provided

EUREKA_QUEST_DISCOVERY_NAME=digital-gatekeeper
QUEST_CONTEXT_PATH=/quest/graphql

#### E5. Eureka registration method :::: Optional ####
# Possible values: direct | route
# Default value: route
# Eureka registration supporting
# :: direct: Direct container access registering IP
# :: route: Route is registered
#
EUREKA_REGISTRATION_METHOD=route
#------------------------------------------------------------------------------
######## L. LOG CONFIGURATION ######## ::: SECTION START
#### L1. Logging level :::: Optional ####
# Supported values are
# error - Only errors are logged
# info - Information logs are enabled
# debug - Debug info is included in the logs
# Default value: info
#
LOG_LEVEL=debug
#### L2. Console logging :::: Optional ####
# Directed to standard output
#
CONSOLE_LOG=true
#### L3. Logs directed to file :::: Required ####
# Directed to file
# Default location is /logs/quest.log
#
FILE_LOG=false
#### L4. File name for File Logs :::: Optional ####
# Will be available in logs folder
#
# FILE_LOG_NAME=quest.log
######## L. LOG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION START
#
#### R1. Include error stack trace :::: Optional ####
# Possible values: true | false
# Default value: false
# Set to true if you require stack traces to be included in logs/ errors section in response
#
ERROR_STACK_TRACE=false
#### R2. Include error path :::: Optional ####
#
ERROR_PATH=true
#### R3. Include timer information in meta :::: Optional ####
# NOTE: To be made optional with default value as false
#
# TIMER=true
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## S. SECURITY CONFIGURATION ######## ::: SECTION START
#### S1. User claims location :::: Optional ####
# Possible values: bearer | header
# Default value: bearer
# :: bearer: bearer id_token in Authorization header
# :: header: custom igtb_headers - igtb_user/igtb_domain
#
USER_CLAIMS_LOCATION=bearer
#### S2. Validate User token :::: Optional ####
# Possible values: true | false
# Default value: true
# Verify signature and expiry of token
# NOTE: Must be able to toggle both of the above independently
#
USER_TOKEN_VALIDATION=false

#### S3. Verify Shared Secret :::: Required ####
# Possible values: true | false
# Verify shared secret for system inquiries (within the delivery tier)
#
VERIFY_SHARED_SECRET=true

######## S. SECURITY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## C. CONFIG CONFIGURATION ######## ::: SECTION START

#### C1. Quest Private Key :::: Required ####
# Used if Loading of externalised configurations is false
igtbPrivateKeys.entl = -----BEGIN PRIVATE KEY-----\nMIIBUwIBADANBgkqhkiG9w0BAQEFAASCAT0wggE5AgEAAkEAwzEq4yDRATlxqxPZITo/Tmhqv3FvsEtfB/n1ia3ZxFBdKPSvWW9A4FhlkmCAOZyuqkSpGXWB4BWpMHhH+a60BwIDAQABAkBLkdSV3NWuUKwmXOfaim+KDrkNZ4CjU3r2Xprmc+10qC9fToTGHnXDPRuoolJfVGtTo9XEJ0AewCnu7PX+oWIpAiEA4gwn8XJHCwc22TjloXqUUOjHrKwDYNxdfqWKG1cjvCsCIQDdDlpOaG5X6cpBVkL5cktX9Cp9kA1xhLNEEXG2JfwNlQIgVpBDJ5IUpKOBnJPOVBVGAOnztSs2K/yXjS1FgwzOsXsCIGez6QeplEJn6jt2lVrilJgBcsGPB89+en7vVBIohbN1AiAeZkMkkvUC8HGLiJUAW7OCpb4ioRX7PqschgHDgWVl5Q==\n-----END PRIVATE KEY-----

#### C2. Quest Certificate :::: Required ####
# Used if Loading of externalised configurations is false
igtbCertificates.entl = -----BEGIN CERTIFICATE-----\nMIIBtjCCAWCgAwIBAgIEXC8IITANBgkqhkiG9w0BAQsFADBiMQswCQYDVQQGEwJJTjELMAkGA1UECAwCVE4xDDAKBgNVBAcMA0NITjESMBAGA1UECgwJSW50ZWxsZWN0MRUwEwYDVQQLDAxpZ3RiLWNieC5jb20xDTALBgNVBAMMBGVudGwwHhcNMTkwMTA0MDcxNTQ1WhcNMjAwMTA0MDcxNTQ1WjBiMQswCQYDVQQGEwJJTjELMAkGA1UECAwCVE4xDDAKBgNVBAcMA0NITjESMBAGA1UECgwJSW50ZWxsZWN0MRUwEwYDVQQLDAxpZ3RiLWNieC5jb20xDTALBgNVBAMMBGVudGwwXDANBgkqhkiG9w0BAQEFAANLADBIAkEAwzEq4yDRATlxqxPZITo/Tmhqv3FvsEtfB/n1ia3ZxFBdKPSvWW9A4FhlkmCAOZyuqkSpGXWB4BWpMHhH+a60BwIDAQABMA0GCSqGSIb3DQEBCwUAA0EAGOWSOKRYV5kMGQIfBfkfkFBVJNCw+PoippWPWqyDE4cW4GmtVjnnLBAaCvdyqk0a1I3NOs5RHo8yPaVvK9407w==\n-----END CERTIFICATE-----

#### C3. IDP Certificate :::: Required ####
# Used if Loading of externalised configurations is false
igtbCertificates.NDFBOTFCQjA3RDMyNDhCM0IyREY3RTJGN0IwMkU4RTk5RDlDODEzNw = -----BEGIN CERTIFICATE-----\nMIIC/zCCAeegAwIBAgIJOqpL6Sw/5g0pMA0GCSqGSIb3DQEBCwUAMB0xGzAZBgNVBAMTEmlndGItY2J4LmF1dGgwLmNvbTAeFw0xNzEyMDUwNjEwMDlaFw0zMTA4MTQwNjEwMDlaMB0xGzAZBgNVBAMTEmlndGItY2J4LmF1dGgwLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMhNIs8y/ylb25wdtyTub0IaagKUbMiLvOeQrXqf5PN0fTSzDItRHbqlWUDv+U4ajzJhND9zyDBefY3ZCxcvC6gIR4blixDtoebjoLW3GxgIdM5DOWxgRFinbZdkpCUgHdvjlXgsPPHQttiTqqRhPFJjHHKaLepqMsD/TAO0KOBLy/UXncKlp1lD6j9ASM+1nycdvitDW31zaUvPFWTFQjnUF84Y9yqhtOJtOHOiZGE2Vz4wLRbGrNYqTcTMZf+R6TqlqgmqOCx9eblqJCjOMfYo+tU0uyLW+1CIexFOT9IUum1KnCZQvMOOrYWbAqb1HCtUAi8s4toYk2T/P+zfJwsCAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUITR+sINi+t7sX4aYeJhQJRjNjO8wDgYDVR0PAQH/BAQDAgKEMA0GCSqGSIb3DQEBCwUAA4IBAQCO6L0TLFrH2QaUtc0cKrLFPwGhqLmIoyP3cYOjjT0ytASEssWzjPJobGNNC5AuRo96tNeC+t4Keh6YhcBrUbuZXQDa2OCM75xY4D/lY3VyXBDAicMZhHLc/urD+LTyWcIljFN6ZIYEgnwkpADzVDSiju+gCn3mVoDvidl8Bk2xNi411GDh20CeF49bz0mrYxXxwKqVtEx6LK2Mam/+4hKR7bgrrReeF06P1XNwMO/YZsZPE2jori7UdhhvDOWivdE2kbGGuvRh7IzY7hs7lOyE57T4z35O29JLABF0E9k/CER2YjvfrDwNf6ttUUIyU/T3AR4I7LuMSMQ2oVa3OzUu\n-----END CERTIFICATE-----

######## C. CONFIG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## N. SUBCRIPTIO[N]S CONFIGURATION ######## ::: SECTION START

#### N1. Subscriptions enabled ? :::: Required ####
# Possible values: true | false
# Default value: true
#
SUBSCRIPTIONS_ENABLED=false

#### N2. Subscription port :::: Optional ####
# Possible values: <Port #> e.g. 4443, 80
# If not specified this will be the same as the App Port
# In CF this is 4443 by default
# In OpenShift - 80
#
SUBSCRIPTION_PORT=80

#### N3. Redis Enabled :::: Required ####
# Possible values: true/false (default: false)
#
REDIS_ENABLED=true

#### N4. Redis Parameters :::: Required ####
# Possible values:
# SVC_HOST_REDIS: <Host name/address> If not specified - localhost
# SVC_PWD_REDIS: <Password> If not specified - null
# SVC_PORT_REDIS: <Port #> If not specified - 6379
#
# SVC_HOST_REDIS=
# SVC_PWD_REDIS=
# SVC_PORT_REDIS=

#### N5. Redis scope :::: Required ####
# For consuming application this will usually be either subscriber or publisher
# For Quest this is expected to be subscriber
# Possible values: subscriber/publisher/all
# Default value: subscriber
#
REDIS_SCOPE=publisher

####N6. Other options
REDIS_CONN_RETRIES=25
REDIS_CONN_NAME=quest
REDIS_KEY_PREFIX=
# Connection retry delay in ms
REDIS_RETRY_DELAY=30000
#### N7. Redis service name :::: Required ####
CF_SVC_NAME_REDIS=redis
REDIS_SUBSCRIPTION_TOPIC=userActivityFeed
######## N. SUBCRIPTIO[N]S CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## X. RABBITMQ CONFIGURATION ######## ::: SECTION START
#
#### X1. RabbitMQ Server URI :::: Required ###
#
MQ_ENABLED=true
KAFKA_ENABLED=false
#SVC_URI_RABBITMQ=
# RMQ_URI=amqp://dituser:dituser@ec2-52-211-125-22.eu-west-1.compute.amazonaws.com:5672/dittest

#### X2. RabbitMQ service name :::: Required ####
# Default value: rabbitmq
# RabbitMQ service name to connect for event publishing
#
CF_SVC_NAME_RABBITMQ=rabbitmq

#### X3. Exchange/DLE/Q/DLQ/Key/DLK Info :::: Required ####
# All values except the exchange will be within the control
# of the base application
# Allow configuration - this will permit setup of exchange/qs/bindings
RMQ_ALLOW_CONFIG=true
# Exchange
RMQ_EXCHANGE=cbxevents
# Prefetch
RMQ_PREFETCH=25
# RMQ Subscription Queue
RMQ_QUEUE=notifications-queue
# RMQ Subscription Key
RMQ_KEY=alert.notification
# RMQ DL Queue
RMQ_DLQ=notifications-dlq
# RMQ_DLKEY:
RMQ_DLKEY=notifications-dlq
RMQ_DLE=cbxevents-dle
# Consumer Tag
RMQ_TAG=quest-notifications
# Rabbit MQ Maximum Retry
RMQ_RETRIES = 5
# Rabbit MQ Retry Delay
RMQ_RETRY_DELAY = 10000
# Rabbit MQ Retry Context
RMQ_REQUEUE_CTX=refreshActivityFeed[0]

QUEST_PACKAGE_TYPE=ingestion
# Ingestion Model
INGESTION_MODEL=NotificationsFeed

######## X. RABBITMQ CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION START

#### M1. Dependant packages :::: Optional ####
# NOTE: This will be phased out
#
# DEP_PKGS=true

#### M2. GraphIql enabled :::: Optional ####
# NOTE: This is for development/test usage
#
GRAPHIQL_ENABLED=true

#### M3. Quest route prefix used in GraphIql :::: Optional ####
# Possible value: /quest
# Default value: N/A (empty)
# Required to support Graphiql route in Gatekeeper
#
# QUEST_ROUTE_PREFIX=

#### M4. Apollo engine configuration :::: Optional ####
# APOLLO_ENGINE=false
# APOLLO_ENGINE_KEY=service:prasvenk007-DigitalQuest:SGRBE4lyBnr4yU4R09W-HA
# APOLLO_TRACING=false
# APOLLO_CACHE_CONTROL=false

#### M5. NewRelic APM Configuration :::: Optional ####
# NEW_RELIC_LICENSE_KEY=9a0343bb1dd3aecf0b2da2e92302b503098cec38

#### M6. Development only :::: Optional ####
# If token validation is not required then custom headers can be provided
# for user identity transmission in Request
#
DEV_USER=DHartm390
DEV_DOMAIN=premierGroup

#### M7. Codacy project token :::: Optional ####
# Required for development
# Integration with Codacy
#
CODACY_PROJECT_TOKEN=9892413b4c5d4ee38f5adf8b5f574446

#### M8. Greeting on Server Start :::: Optional ####
# GREETING=Welcome

######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
######## N. NOTIFICATION CONFIGURATION ######## ::: SECTION START
#### N1. SMTP Enabled :::: Required ####
# Possible values: true | false
# Default value: false
# 
SMTP_ENABLED=true
CHANNEL_SMTP=email

#### N2. SMS RETRY :::: Optional ####
# Retry email api upto 5 times for single user
# Default value: 5
# 
# SMTP_RETRIES=5

#### N3. SMS Enabled :::: Required ####
# Possible values: true | false
# Default value: false
# 
SMS_ENABLED=true
CHANNEL_SMS=sms

#### N4. SMS RETRY :::: Optional ####
# Retry sms api upto 5 times for single user
# Default value: 5
# 
# SMS_RETRIES=5

#### N5. SMTP API :::: Required ####
# Send email to user by using this config
# Local value for verify
SVC_URI_SMTP=smtp://appserver01.irelease.igtb.digital:2525
# Secure email for verify
# SVC_URI_SMTP=smtps://submit.notes.ap.collabserv.com:465
SMTP_USERNAME=igtbdigital.dev@intellectdesign.com
# SMTP_PASSWORD=Welcome@01
# Interface api
# SVC_URI_SMTP=smtp://appserver01.irelease.igtb.digital:2525
# SMTP_USERNAME=info@intellectdesign.com

#### N6. PROXY EMAIL ID :::: Optional ####
# Send email to default user
# 
# PROXY_EMAIL=sankalp.saxena@intellectdesign.com

#### N7. SMS Enabled :::: Required ####
# Possible values: true | false
# Default value: false
# 
MOBILE_ENABLED=true
CHANNEL_MOBILE=mobpush
CHANNEL_WEB=push

#### N7.1 FCM API :::: Optional ####
# Send push notifications to mobile (android)
# 
FCM_API=https://fcm.googleapis.com/fcm/send
FCM_AUTH=key=AIzaSyAZ3MN9Q_reh73_X-1KKUx67sGE05XjDSU

### Send push notifications to mobile (ios)
#### N7.2. APNS Certificate Location :::: Required ####
#
APNS_CERT_LOCATION=certs/AuthKey_TJKERBYB6T.p8

#### N7.3. APN Key Id  :::: Required ####
# 
APN_KEYID=TJKERBYB6T

#### N7.4. APN Team Id  :::: Required ####
# 
APN_TEAMID=ZMAN7BEA32

#### N7.5. APN Topic  :::: Required ####
# 
APN_TOPIC=com.intellect.bbl

######## N. NOTIFICATION CONFIGURATION ######## ::: SECTION END
#-----------------------------------------------------------------------------------
######## I. APPLICATION EXTERNAL INTERFACE CONFIGURATION ######## ::: SECTION START
#### I1. BBL SMS Interface API :::: Required ####
# Possible values: interface api
# Default value: interface api
#
SMS_API=http://appserver01.irelease.igtb.digital:20184/MH/RESTWS/ServicePost

######## I. APPLICATION EXTERNAL INTERFACE CONFIGURATION ######## ::: SECTION END
#-----------------------------------------------------------------------------------
# 

# Log Event Message
LOG_MESSAGES=true
# GATEKEEPER_URI=http://digital-gatekeeper-internal-service-cbx.apps.irelease-openshift.us-east-1.igtb.digital
DEFAULT_WORKFLOW_ERROR_MESSAGE=There is a problem with the approval workflow setup

USER_PREFERRED_ALERTS=false